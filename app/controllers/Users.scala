package controllers

import com.google.inject.Inject
import controllers.utils.UserAction
import models.{Authenticator, JsonValidationError}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsError, JsPath, Json, Reads}
import play.api.mvc._


class Users @Inject() (
  auth: Authenticator,
  userAction: UserAction
) extends Controller {


  case class RegisterParams(email: String, firstName: String, lastName: String, password: String)
  implicit val registerParamsReads: Reads[RegisterParams] = (
    (JsPath \ "email").read[String](Reads.email) and
    (JsPath \ "firstName").read[String](Reads.minLength[String](1)) and
    (JsPath \ "lastName").read[String](Reads.minLength[String](1)) and
    (JsPath \ "password").read[String](Reads.minLength[String](4))
  ) (RegisterParams.apply _)

  def register() = Action.async(parse.json) { req =>
    req.body.validate[RegisterParams].map { params =>
      auth.register(params.email, params.firstName, params.lastName, params.password).map {
        case (user, session) => Ok(Json.obj(
          "user" -> user,
          "session" -> session
        )).withCookies(
          Cookie("sessionId", session.token, maxAge = Some(session.ttl))
        )
      }
    }.recoverTotal {
      case e: JsError => throw JsonValidationError(e)
    }
  }


  case class AuthenticateParams(email: String, password: String)
  implicit val authenticateParamsReads: Reads[AuthenticateParams] = (
    (JsPath \ "email").read[String](Reads.email) and
    (JsPath \ "password").read[String](Reads.minLength[String](4))
  ) (AuthenticateParams.apply _)

  def authenticate() = Action.async(parse.json) { req =>
    req.body.validate[AuthenticateParams].map { p =>
      auth.authenticate(p.email, p.password)
        .map { session =>
          Ok(Json.toJson(session))
            .withCookies(Cookie("sessionId", session.token, maxAge = Some(session.ttl)))
        }
    }.recoverTotal {
      case e: JsError => throw JsonValidationError(e)
    }
  }

  def me = userAction { req =>
    Ok(Json.toJson(req.user))
  }

}
