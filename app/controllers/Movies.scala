package controllers

import com.google.inject.Inject
import controllers.utils.{AuthAction, PublicAction, UserAction}
import models.services.RecommendationsService
import models.{JsonValidationError, Movie, MovieProvider, Rating}
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsError, Json}
import play.api.mvc._

import scala.async.Async._


class Movies @Inject() (
  movies: MovieProvider,
  recommendations: RecommendationsService,
  userAction: UserAction,
  authAction: AuthAction,
  publicAction: PublicAction
) extends Controller {

  def topRated(count: Int) = publicAction.async { req =>

    def res(movieList: Seq[(Movie, Double)], ratings: Seq[Rating]) = {
      val jsObjects = movieList.map {
        case (movie, weight) =>
          val userRating = ratings.find(r => r.movieId == movie.id).map(_.rating)
          Json.obj(
            "weight" -> weight,
            "movie" -> Json.toJson(movie.copy(rating = userRating))
          )
      }
      Ok(Json.toJson(jsObjects))
    }

    val topRatedMoviesFuture = movies.topRated(count)

    req.clientSession match {
      case Some(session) => async {
        val m = await { topRatedMoviesFuture }
        val ratings = await { recommendations.getMoviesRatedByUser(session.userId, m.map(_._1.id)) }
        res(m, ratings)
      }

      case None => async {
        val m = await { topRatedMoviesFuture }
        res(m, Seq.empty[Rating])
      }
    }
  }

  def get(id: Int) = Action.async {
    movies.get(id).map {
      case Some(movie) => Ok(Json.toJson(movie))
      case None => NotFound(Json.obj("error" -> s"No movie with id = $id"))
    }
  }

  case class RateParams(movieId: Int, rating: Double)
  implicit val rateParamsJsonFormat = Json.format[RateParams]

  def rate(movieId: Int) = authAction.async(parse.json) { req =>
    Logger.info(req.toString())
    req.body.validate[RateParams].map { params =>
      recommendations.updateRating(req.clientSession.userId, params.movieId, params.rating).map { rating =>
        Ok(Json.toJson(rating))
      }
    }.recoverTotal {
      case e: JsError => throw JsonValidationError(e)
    }

  }

  def deleteRating(movieId: Int) = authAction.async { req =>
    recommendations.deleteRating(req.clientSession.userId, movieId).map { _ =>
      Ok(Json.obj("message" -> s"rating ${req.clientSession.userId} -> $movieId removed"))
    }
  }

  def recommendedForMe() = authAction.async { req =>
    async {
      val movieList = await { movies.recommendedFor(req.clientSession.userId) }
      val ratings = await {
        recommendations.getMoviesRatedByUser(req.clientSession.userId, movieList.map(_._1.id))
      }

      val jsObjects = movieList.map {
        case (movie, weight) =>
          val rating: Double = ratings
            .find(r => r.movieId == movie.id)
            .map(r => r.rating)
            .getOrElse(0.0)

          Json.obj(
            "weight" -> weight,
            "movie" -> Json.toJson(movie.copy(rating = Some(rating)))
          )
      }
      Ok(Json.toJson(jsObjects))
    }
  }

  case class PaginatedResult(values: Seq[Movie], page: Int, count: Int, next: Option[String])
  implicit val paginatedResultJsonFormat = Json.format[PaginatedResult]

  def search(title: String, page: Int, pageLen: Int) = publicAction.async { req =>
    val offset = (page - 1) * pageLen
    async {
      val foundMovies = await { movies.search(req.clientSession.map(_.userId), title, offset, pageLen) }
      val next = if (foundMovies.length < pageLen) None
                 else Some(routes.Movies.search(title, page + 1, pageLen).url.toString)
      val paginatedResult = PaginatedResult(foundMovies, page, foundMovies.length, next)
      Ok(Json.toJson(paginatedResult))
    }
  }

}
