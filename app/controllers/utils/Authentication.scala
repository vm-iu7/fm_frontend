package controllers.utils

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models.{AuthenticationException, Authenticator, Session, User}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc._

import scala.concurrent.Future

class UserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

@ImplementedBy(classOf[UserActionImpl])
trait UserAction extends ActionBuilder[UserRequest] {}

@Singleton
class UserActionImpl @Inject() (
  authenticator: Authenticator
) extends UserAction {

  override def invokeBlock[A] (
    request: Request[A],
    block: (UserRequest[A]) => Future[Result])
  : Future[Result] = {
    request.cookies.get("sessionId") match {

      case Some(sessionIdCookie) =>
        val sessionId = sessionIdCookie.value
        authenticator.me(sessionId).flatMap {
          case Some(user) => block(new UserRequest[A](user, request))
          case None => Future.failed(new AuthenticationException("no session with given id"))
        }

      case None => Future.failed(new AuthenticationException("no sessionId cookie"))
    }
  }
}


class SessionRequest[A](val clientSession: Session, request: Request[A]) extends WrappedRequest[A](request)

@ImplementedBy(classOf[AuthActionImpl])
trait AuthAction extends ActionBuilder[SessionRequest] {}

@Singleton
class AuthActionImpl @Inject() (
  authenticator: Authenticator
) extends AuthAction {

  override def invokeBlock[A](
    request: Request[A], block: (SessionRequest[A]) => Future[Result])
  : Future[Result] = {
    request.cookies.get("sessionId") match {
      case Some(sessionIdCookie) =>
        val sessionId = sessionIdCookie.value
        authenticator.session(sessionId).flatMap {
          case Some(session) => block(new SessionRequest[A](session, request))
          case None => throw AuthenticationException("session with given id not found")
        }
      case None => throw AuthenticationException("sessionId cookie not found")
    }
  }

}

class PublicRequest[A](val clientSession: Option[Session], request: Request[A]) extends WrappedRequest[A](request)

@ImplementedBy(classOf[PublicActionImpl])
trait PublicAction extends ActionBuilder[PublicRequest] {}

@Singleton
class PublicActionImpl @Inject() (authenticator: Authenticator) extends PublicAction {
  override def invokeBlock[A](
    request: Request[A], block: (PublicRequest[A]) => Future[Result])
  : Future[Result] = {
    request.cookies.get("sessionId").map { cookie =>
      authenticator.session(cookie.value).flatMap { sessionOpt =>
        block(new PublicRequest(sessionOpt, request))
      }
    }.getOrElse {
      block(new PublicRequest(Option.empty[Session], request))
    }
  }
}