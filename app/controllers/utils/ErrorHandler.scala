package controllers.utils

import java.io.{PrintWriter, StringWriter}

import models.{JsonValidationError, AuthenticationException}
import models.services.ServiceErrorResponse
import play.api.Logger
import play.api.http.HttpErrorHandler
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._

import scala.concurrent._


class ErrorHandler extends HttpErrorHandler {

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(
      Status(statusCode)(Json.obj("error" -> message))
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    Future.successful {
      exception match {
        case e @ ServiceErrorResponse(status, message, jsonAns) => InternalServerError(Json.obj(
          "error" -> message,
          "details" -> jsonAns
        ))

        case e: AuthenticationException =>
          Logger.info(s"Authentication failed for request $request: ${e.details}")
          Unauthorized(Json.obj("error" -> s"Authentication error"))

        case e: JsonValidationError => BadRequest(Json.obj(
          "error" -> "Parameters validation error",
          "details" -> e.toJson
        ))

        case e =>
          val sw = new StringWriter()
          val pw = new PrintWriter(sw)
          e.printStackTrace(pw)
          InternalServerError(Json.obj(
            "error" -> exception.getMessage,
            "stacktrace" -> sw.toString
          ))
      }
    }
  }
}
