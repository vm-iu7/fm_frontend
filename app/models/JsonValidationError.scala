package models

import play.api.libs.json.JsError

case class JsonValidationError(err: JsError) extends Exception(err.toString) {
  def toJson = JsError.toJson(err)
}
