package models

import scala.concurrent.{Future, ExecutionContext}

import com.google.inject.{Inject, ImplementedBy}

import play.api.Configuration
import play.api.libs.json.Json

import models.services.{MoviesService, RecommendationsService}

case class Rating(userId: Int, movieId: Int, rating: Double)

object Rating {
  implicit val jsonFormat = Json.format[Rating]
}
