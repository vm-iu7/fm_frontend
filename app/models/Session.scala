package models

import play.api.libs.json.Json


case class Session(token: String, userId: Int, ttl: Int)

object Session {
  implicit val jsonFormat = Json.format[Session]
}
