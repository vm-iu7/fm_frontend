package models

import scala.concurrent.{Future, ExecutionContext}
import scala.async.Async._

import com.google.inject.{Inject, ImplementedBy}

import play.api.Configuration
import play.api.libs.json.Json

import models.services.{MoviesService, RecommendationsService}


case class Movie(
  id: Int,
  name: String,
  year: Int,
  description: Option[String] = None,
  rating: Option[Double] = None,
  tmdbId: Option[Int] = None,
  tmdbPosterPath: Option[String] = None
)

object Movie {
  implicit val jsonFormat = Json.format[Movie]
}

@ImplementedBy(classOf[MovieProviderImpl])
trait MovieProvider {
  def topRated(count: Int)(implicit ec: ExecutionContext): Future[Seq[(Movie, Double)]]
  def get(id: Int)(implicit ec: ExecutionContext): Future[Option[Movie]]
  def recommendedFor(userId: Int)(implicit ec: ExecutionContext): Future[Seq[(Movie, Double)]]
  def search(userId: Option[Int], title: String, offset: Int, limit: Int)
            (implicit ec: ExecutionContext): Future[Seq[Movie]]
}

class MovieProviderImpl @Inject() (
  config: Configuration,
  moviesService: MoviesService,
  recommendationsService: RecommendationsService
) extends MovieProvider {

  def topRated(count: Int)(implicit ec: ExecutionContext): Future[Seq[(Movie, Double)]] = async {
    val topRatedRecs = await { recommendationsService.getTopRatedMovies(count) }
    val topRatedMovies = await { moviesService.getList(topRatedRecs.map(_.movieId).toList) }

    topRatedRecs.zip(topRatedMovies).map {
      case (Recommendation(_, _, weight), movie) => movie -> weight
    }
  }

  def get(id: Int)(implicit ec: ExecutionContext): Future[Option[Movie]] = {
    moviesService.get(id)
  }

  def recommendedFor(userId: Int)(implicit ec: ExecutionContext)
  : Future[Seq[(Movie, Double)]] = async {
    val recs = await { recommendationsService.getRecommendations(userId) }
    val movieIds = recs.map(_.movieId).toList
    val recommendedMovies = await { moviesService.getList(movieIds.toList) }

    recs.zip(recommendedMovies).map {
      case (Recommendation(_, _, weight), movie) => movie -> weight
    }
  }

  def search(userId: Option[Int], title: String, offset: Int, limit: Int)
            (implicit ec: ExecutionContext): Future[Seq[Movie]] = async {
    val foundMovies = await { moviesService.search(title, offset, limit) }
    val ratings = userId match {
      case Some(uid) =>
        await {
          recommendationsService
            .getMoviesRatedByUser(uid, foundMovies.map(_.id))
            .recover {
              case e => Seq.empty[Rating]
            }
        }

      case None => Seq.empty[Rating]
    }

    val moviesWithRatings =
      if (ratings.isEmpty) foundMovies
      else {
        foundMovies.map { m =>
          val r = ratings.find(r => r.movieId == m.id).map(_.rating)
          m.copy(rating = r)
        }
      }

    moviesWithRatings
  }

}
