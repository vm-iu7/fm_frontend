package models

import com.google.inject.{ImplementedBy, Inject}
import models.services.{SessionService, UsersService}
import play.api.Logger

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[AuthenticatorImpl])
trait Authenticator {
  def register (email: String, firstName: String, lastName: String, password: String)
               (implicit ec: ExecutionContext): Future[(User, Session)]

  def authenticate (email: String, password: String)
                   (implicit ec: ExecutionContext): Future[Session]

  def me (sessionId: String) (implicit ec: ExecutionContext): Future[Option[User]]

  def session(sessionId: String) (implicit ec: ExecutionContext): Future[Option[Session]]
}

class AuthenticatorImpl @Inject() (
  users: UsersService,
  session: SessionService
) extends Authenticator {

  override def register (email: String, firstName: String, lastName: String, password: String)
                        (implicit ec: ExecutionContext): Future[(User, Session)] = {
    users.createUser(email, firstName, lastName, password).flatMap { user =>
      session.postUser(user.id, email, password).flatMap { _ =>
        session.postSession(email, password).map { s =>
          user -> s
        }
      }.recover {
        case e =>
          Logger.error("Failed to create user at session service. Removing user from Users service..")
          // TODO: delete user from users service
          throw e;
      }
    }
  }

  override def authenticate (email: String, password: String)
                            (implicit ec: ExecutionContext): Future[Session] = {
    session.postSession(email, password)
  }

  override def me (sessionId: String) (implicit ec: ExecutionContext): Future[Option[User]] = {
    session.validateToken(sessionId).flatMap {
      case Some(s) => users.getUser(s.userId)
      case None => Future(None)
    }
  }

  override def session(sessionId: String)(implicit ec: ExecutionContext): Future[Option[Session]] = {
    session.validateToken(sessionId)
  }
}

case class AuthenticationException(details: String) extends Exception(details)
