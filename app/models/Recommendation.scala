package models

import play.api.libs.json.Json


case class Recommendation(userId: Int, movieId: Int, weight: Double)

object Recommendation {
  implicit val recommendationJsonFormat = Json.format[Recommendation]
}
