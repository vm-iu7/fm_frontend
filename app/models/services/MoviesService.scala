package models.services

import com.google.inject.{ImplementedBy, Inject}
import models.Movie
import play.api.Configuration
import play.api.libs.ws._

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[MoviesServiceImpl])
trait MoviesService {

  def get(id: Int)(implicit ec: ExecutionContext): Future[Option[Movie]]

  def getList(ids: List[Int])(implicit ec: ExecutionContext): Future[Seq[Movie]]

  def search(title: String, offset: Int, limit: Int)(implicit ec: ExecutionContext): Future[Seq[Movie]]

}

class MoviesServiceImpl @Inject() (
  ws: WSClient,
  config: Configuration
) extends MoviesService {

  def error[A](response: WSResponse) : A = {
    throw ServiceErrorResponse(
      response.status, s"Got ${response.status} from movies service", Some(response.json)
    )
  }

  val baseUrl = (for {
    cn <- config.getConfig("services.movies")
    host <- cn.getString("host")
  } yield {
    s"http://$host"
  }).getOrElse {
    throw new IllegalArgumentException("Failed to configure MoviesService. Check configuration file")
  }

  def get(id: Int)(implicit ec: ExecutionContext): Future[Option[Movie]] = {
    ws.url(s"$baseUrl/movies/$id")
      .get()
      .map { response =>
        response.status match {
          case 200 => Some(response.json.as[Movie])
          case 404 => None
          case _ => error(response)
        }
      }
  }

  def getList(ids: List[Int])(implicit ec: ExecutionContext): Future[Seq[Movie]] = {
    val idsQuery = ids.map(id => s"ids=$id").mkString("&")
    ws.url(s"$baseUrl/movies/list?$idsQuery")
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Movie]]
          case _ => error(response)
        }
      }
  }

  def search(title: String, offset: Int, limit: Int)(implicit ec: ExecutionContext): Future[Seq[Movie]] = {
    ws.url(s"$baseUrl/movies")
      .withQueryString(
        "title" -> title,
        "offset" -> offset.toString,
        "limit" -> limit.toString
      )
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Movie]]
          case _ =>  error(response)
        }
      }
  }

}
