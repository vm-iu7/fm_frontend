package models.services

import play.api.libs.json.JsValue

case class ServiceErrorResponse(
  statusCode: Int,
  message: String,
  answerJson: Option[JsValue] = None
) extends Exception(message)
