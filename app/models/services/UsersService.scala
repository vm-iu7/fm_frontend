package models.services

import com.google.inject.{ImplementedBy, Inject}
import models.User
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws._

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[UsersServiceImpl])
trait UsersService {

  def getUser(id: Int)(implicit ec: ExecutionContext): Future[Option[User]]

  def createUser(email: String, firstName: String, lastName: String, password: String)
                        (implicit ec: ExecutionContext): Future[User]

  def getUserByEmail(email: String)(implicit ec: ExecutionContext): Future[Option[User]]

}

class UsersServiceImpl @Inject() (
  ws: WSClient,
  conf: Configuration
) extends UsersService {

  val baseUrl = (for {
    cn <- conf.getConfig("services.users")
    host <- cn.getString("host")
  } yield {
    s"http://$host"
  }).getOrElse {
    throw new IllegalArgumentException("Failed to configure UsersService. Check configuration file")
  }

  def error[A](response: WSResponse): A = {
    throw ServiceErrorResponse(
      response.status, s"Got ${response.status} from Users service", Some(response.json)
    )
  }

  override def getUser(id: Int)(implicit ec: ExecutionContext): Future[Option[User]] = {
    ws.url(s"$baseUrl/users/$id")
      .get()
      .map { response =>
        response.status match {
          case 200 => Some(response.json.as[User])
          case 404 => None
          case _ => error(response)
        }
      }
  }

  override def createUser(email: String, firstName:String, lastName: String, password: String)
                         (implicit ec: ExecutionContext): Future[User] = {

    val data = Json.obj(
      "email" -> email,
      "firstName" -> firstName,
      "lastName" -> lastName,
      "password" -> password
    )

    ws.url(s"$baseUrl/users")
      .post(data)
      .map { response =>
        response.status match {
          case 200 => response.json.as[User]
          case _ => error(response)
        }
      }
  }

  def getUserByEmail(email: String)(implicit ec: ExecutionContext): Future[Option[User]] = {
    ws.url(s"$baseUrl/users")
      .withQueryString("email" -> email)
      .get()
      .map { response =>
        response.status match {
          case 200 => Some(response.json.as[User])
          case 404 => None
          case _ => error(response)
        }
      }
  }

}
