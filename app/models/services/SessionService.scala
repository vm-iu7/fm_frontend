package models.services

import com.google.inject.{ImplementedBy, Inject}
import models.{AuthenticationException, Session}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws._

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[SessionServiceImpl])
trait SessionService {

  def postSession(email: String, password: String) (implicit ec: ExecutionContext): Future[Session]

  def validateToken(sessionId: String) (implicit ec: ExecutionContext): Future[Option[Session]]

  def postUser(id: Int, email: String, password: String) (implicit ec: ExecutionContext): Future[Unit]

}

case class SessionUser(id: Int, email: String, password: String)

class SessionServiceImpl @Inject() (
  config: Configuration,
  ws: WSClient
) extends SessionService {

  val baseUrl = (for {
    cn <- config.getConfig("services.session")
    host <- cn.getString("host")
  } yield {
    s"http://$host"
  }).getOrElse {
    throw new IllegalArgumentException("Failed to configure SessionsService. Check configuration file")
  }

  def error[A](response: WSResponse): A = {
    throw ServiceErrorResponse(
      response.status, s"Got ${response.status} from session service", Some(response.json)
    )
  }

  def postSession(email: String, password: String) (implicit ec: ExecutionContext): Future[Session] = {
    val data = Json.obj("email" -> email, "password" -> password)
    ws.url(s"$baseUrl/sessions")
      .post(data)
      .map { response =>
        response.status match {
          case 200 => response.json.as[Session]
          case code if 400 until 500 contains code =>
            throw new AuthenticationException("Incorrect email and/or password")
          case _ => error(response)
        }
      }
  }

  def postUser(id: Int, email: String, password: String) (implicit ec: ExecutionContext)
  : Future[Unit] = {
    val data = Json.obj(
      "id" -> id,
      "email" -> email,
      "password" -> password
    )

    ws.url(s"$baseUrl/users")
      .post(data)
      .map { response =>
        response.status match {
          case ok if 200 until 300 contains ok => Unit
          case _ => error(response)
        }
      }
  }

  def validateToken(token: String) (implicit ec: ExecutionContext): Future[Option[Session]] = {
    ws.url(s"$baseUrl/sessions/$token")
      .get()
      .map { response =>
        response.status match {
          case 200 => Some(response.json.as[Session])
          case 404 => None
          case _ => error(response)
        }
      }
  }

}
