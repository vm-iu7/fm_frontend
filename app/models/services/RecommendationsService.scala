package models.services

import com.google.inject.{ImplementedBy, Inject}
import models.{Rating, Recommendation}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws._

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[RecommendationsServiceImpl])
trait RecommendationsService {

  def getTopRatedMovies(count: Int)
      (implicit ec: ExecutionContext): Future[Seq[Recommendation]]

  def getMostPopularMovies(count: Int)
      (implicit ec: ExecutionContext): Future[Seq[Recommendation]]

  def getRecommendations(userId: Int)(implicit ec: ExecutionContext): Future[Seq[Recommendation]]

  def getRating(userId: Int, movieId: Int)
      (implicit ec: ExecutionContext): Future[Option[Rating]]

  def getRatings(userId: Int)
      (implicit ec: ExecutionContext): Future[Seq[Rating]]

  def getMoviesRatedByUser(userId: Int, movieIds: Seq[Int])
      (implicit ec: ExecutionContext): Future[Seq[Rating]]

  def updateRating(userId: Int, movieId: Int, rating: Double)
      (implicit ec: ExecutionContext): Future[Rating]

  def deleteRating(userId: Int, movieId: Int)
      (implicit ec: ExecutionContext): Future[Unit]

  def error[A](response: WSResponse): A = {
    throw ServiceErrorResponse(
      response.status, s"got ${response.status} from recommendations", Some(response.json)
    )
  }

}

class RecommendationsServiceImpl @Inject() (
  ws: WSClient,
  config: Configuration
) extends RecommendationsService {

  val baseUrl = (for {
    cn <- config.getConfig("services.recommendations")
    host <- cn.getString("host")
  } yield {
    s"http://$host"
  }).getOrElse {
    throw new IllegalArgumentException("Failed to configure RecommendationsService. Check configuration file")
  }

  def getTopRatedMovies(count: Int)
                       (implicit ec: ExecutionContext): Future[Seq[Recommendation]] = {
    ws.url(s"$baseUrl/movies/top_rated")
      .withQueryString("count" -> count.toString)
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Recommendation]]
          case _ => error(response)
        }
      }
  }

  def getRecommendations(userId: Int)(implicit ec: ExecutionContext)
  : Future[Seq[Recommendation]] = {
    ws.url(s"$baseUrl/users/$userId/recommendations")
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Recommendation]]
          case _ => error(response)
        }
      }
  }

  def getMostPopularMovies (count: Int)
                           (implicit ec: ExecutionContext): Future[Seq[Recommendation]] = {
    ws.url(s"$baseUrl/movies/top_rated")
      .withQueryString("count" -> count.toString)
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Recommendation]]
          case _ => error(response)
        }
      }
  }

  override def getRating(userId: Int, movieId: Int)
                        (implicit ec: ExecutionContext): Future[Option[Rating]] = {
    ws.url(s"$baseUrl/users/$userId/ratings/$movieId")
      .get()
      .map { response =>
        response.status match {
          case 200 => Some(response.json.as[Rating])
          case 404 => None
          case code => error(response)
        }
      }
  }


  override def getMoviesRatedByUser(userId: Int, movieIds: Seq[Int])
      (implicit ec: ExecutionContext): Future[Seq[Rating]] = {
    val moviesQuery = movieIds.map(m => s"movies=$m").mkString("&")
    ws.url(s"$baseUrl/users/$userId/ratings/list?$moviesQuery")
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Rating]]
          case code => error(response)
        }
      }
  }

  override def getRatings(userId: Int)(implicit ec: ExecutionContext): Future[Seq[Rating]] = {
    ws.url(s"$baseUrl/users/$userId/ratings")
      .get()
      .map { response =>
        response.status match {
          case 200 => response.json.as[Seq[Rating]]
          case 404 => Seq.empty[Rating]
          case code => error(response)
        }
      }
  }

  def updateRating(userId: Int, movieId: Int, rating: Double)
                  (implicit ec: ExecutionContext): Future[Rating] = {
    val data = Json.obj("rating" -> rating)
    ws.url(s"$baseUrl/users/$userId/ratings/$movieId")
      .put(data)
      .map { response =>
        response.status match {
          case 200 => response.json.as[Rating]
          case code => error(response)
        }
      }
  }

  def deleteRating(userId: Int, movieId: Int)
                  (implicit ec: ExecutionContext): Future[Unit] = {
    ws.url(s"$baseUrl/users/$userId/ratings/$movieId")
      .delete()
      .map { response =>
        response.status match {
          case 200 => Unit
          case code => error(response)
        }
      }
  }
}
