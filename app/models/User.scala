package models

import play.api.libs.json.Json


case class User(id: Int, email: String, firstName: String, lastName: String)

object User {
  implicit val jsonFormat = Json.format[User]
}
