package fakes

import controllers.utils._
import models.{Session, User}
import play.api.mvc.{Request, Result}

import scala.concurrent.Future

object FakeAuthAction {
  val userId = 1
}

class FakeAuthAction extends AuthAction {
  override def invokeBlock[A](
    request: Request[A], block: (SessionRequest[A]) => Future[Result])
  : Future[Result] = {
    block(new SessionRequest[A](Session("testtoken", FakeAuthAction.userId, 100), request))
  }
}

class FakeUserAction extends UserAction {
  override def invokeBlock[A](
    request: Request[A], block: (UserRequest[A]) => Future[Result])
  : Future[Result] = {
    block(new UserRequest[A](User(1, "test@test.com", "Test", "Name"), request))
  }
}

class FakePublicAction extends PublicAction {
  override def invokeBlock[A](request: Request[A], block: (PublicRequest[A]) => Future[Result])
  : Future[Result] = {
    block(new PublicRequest[A](None, request))
  }
}
