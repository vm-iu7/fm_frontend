import controllers.utils.AuthAction

import org.junit.runner._
import org.specs2.execute.{AsResult, Result}
import org.specs2.mock.Mockito
import org.specs2.mutable._
import org.specs2.runner._
import org.specs2.specification.Scope

import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test._

import scala.concurrent.Future

import fakes.{FakePublicAction, FakeAuthAction}
import models.{Movie, MovieProvider}

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification with Mockito {

  trait TestSetup extends Around with Scope {
    val mp = mock[MovieProvider]
    val authAction = new FakeAuthAction()
    val publicAction = new FakePublicAction()
    val appMock = new GuiceApplicationBuilder()
      .overrides(bind[MovieProvider].toInstance(mp))
      .overrides(bind[AuthAction].toInstance(authAction))
      .build()
    implicit def implicitApp = appMock
    override def around[T: AsResult](t: => T): Result = {
      Helpers.running(appMock)(AsResult.effectively(t))
    }
  }

  "Application" should {

    "send 404 on a bad request" in new WithApplication {
      route(FakeRequest(GET, "/boum")) must beSome.which(status(_) == NOT_FOUND)
    }

    "render the index page" in new WithApplication {
      val home = route(FakeRequest(GET, "/")).get

      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ == "text/html")
    }



    "movies controller" should {
      "return top rated movies in json" in new TestSetup {
        mp.topRated(2) returns Future.successful(Seq(
          Movie(1, "test movie 1", 2015) -> 5.0,
          Movie(2, "test movie 2", 2015) -> 4.0
        ))

        val topRatedRoute = route(FakeRequest(GET, "/movies/top_rated?count=2")).get

        status(topRatedRoute) must equalTo(OK)
        contentType(topRatedRoute) must beSome.which(_ == "application/json")

        val expected = Json.arr(
          Json.obj("movie" -> Json.toJson(Movie(1, "test movie 1", 2015)), "weight" -> 5.0),
          Json.obj("movie" -> Json.toJson(Movie(2, "test movie 2", 2015)), "weight" -> 4.0)
        )
        contentAsJson(topRatedRoute) must equalTo(Json.toJson(expected))
      }

      "return movie by id if exists" in new TestSetup {
        mp.get(2) returns Future.successful(Some(Movie(2, "Test Movie", 2015)))
        val req = route(FakeRequest(GET, "/movies/2")).get

        there was one(mp).get(2)

        status(req) must equalTo(OK)
        contentType(req) must beSome.which(_ == "application/json")

        val expected = Json.toJson(Movie(2, "Test Movie", 2015))
        contentAsJson(req) must equalTo(expected)
      }

      "return 404 if movie not found" in new TestSetup {
        mp.get(2) returns Future.successful(None)
        val req = route(FakeRequest(GET, "/movies/2")).get

        there was one(mp).get(2)

        status(req) must equalTo(NOT_FOUND)
        contentType(req) must beSome.which(_ == "application/json")
      }

//      "find movies by title" in new TestSetup {
//        val fakeRes = Seq(
//          Movie(1, "test movie 1", 2015),
//          Movie(2, "test movie 2", 2015)
//        )
//
//        mp.search(any, "test", 0, 2) returns Future.successful(fakeRes)
//
//        var req = route(FakeRequest(GET, "/movies?title=test&page=1&page_len=2")).get
//
//        there was one (mp).search(None, "test", 0, 2)
//
//        status(req) must equalTo(OK)
//        contentType(req) must beSome.which(_ == "application/json")
//
//        val expectedJson = Json.obj(
//          "page" -> 0,
//          "page_len" -> 2,
//          "values" -> Json.arr(
//            Json.obj("id" -> 1, "title" -> "test movie 1", "year" -> 2015),
//            Json.obj("id" -> 2, "title" -> "test movie 2", "year" -> 2015)
//          )
//        )
//
//        contentAsJson(req) mustEqual expectedJson
//      }
    }
  }
}
