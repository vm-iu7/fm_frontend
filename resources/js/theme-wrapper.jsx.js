'use strict';

var Theme = require('./dark_theme.jsx.js');
var ColorManipulator = require('material-ui/lib/utils/color-manipulator');


module.exports = {
  theme: Theme,
  palette: Theme.palette,
  tabStyle: {
    color: Theme.palette.textColor,
    backgroundColor: ColorManipulator.darken(Theme.palette.canvasColor, 0.4)
  }
};
