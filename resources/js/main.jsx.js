'use strict';

var React                   = require('react'),
    injectTapEventPlugin    = require("react-tap-event-plugin"),
    ReactDOM                = require('react-dom'),

    Snackbar                = require('material-ui/lib/snackbar'),
    Tab                     = require('material-ui/lib/tabs/tab'),
    Tabs                    = require('material-ui/lib/tabs/tabs'),
    Paper                   = require('material-ui/lib/paper'),
    ThemeManager            = require('material-ui/lib/styles/theme-manager'),
    ColorManipulator        = require('material-ui/lib/utils/color-manipulator'),

    PopularTab              = require('./components/popular_tab.jsx.js'),
    RecommendationsTab      = require('./components/recommendations_tab.jsx.js'),
    SearchTab               = require('./components/search_tab.jsx.js'),
    AuthDialog              = require('./components/auth_dialog.jsx.js'),
    UserAvatar              = require('./components/user_avatar.jsx.js'),
    MyTheme                 = require('./theme-wrapper.jsx.js');


//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

var Main = React.createClass({

  componentDidMount: function() {
    $('body').css('background-color', MyTheme.palette.canvasColor);

    $(document).ajaxError(function(e, x) {
      console.log(x);
      this.setState({snackbarMessage: ["(", x.status + ") " + x.responseJSON.error].join("")});
      this.refs.ratedSnackbar.show();
      if (x.status == 403) {
        this.refs.authDialog.show();
      }
    }.bind(this));

    var req = $.get('/users/me');

    req.done(function (me) {
      this.setState({user: me})
    }.bind(this));

  },

  getInitialState: function() {
    return {
      user: undefined,
      snackbarMessage: "",
      tab: "recommendations"
    }
  },

  //the key passed through context must be called "muiTheme"
  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getChildContext() {
    return {
      muiTheme: ThemeManager.getMuiTheme(MyTheme.theme),
    };
  },

  onUserAvatarClicked: function() {
    this.refs.authDialog.show();
  },

  onRated: function(rating, movie, ackCallback) {
    if (rating != 0) {
      this.postRating(movie, rating, ackCallback);
    } else {
      this.deleteRating(movie, ackCallback);
    }
  },

  postRating: function(movie, rating, ackCallback) {
    var params = {
      movieId: movie.id,
      rating: rating
    };

    var request = $.ajax({
      method: 'PUT',
      url: ['/movies/', movie.id,'/rating'].join(''),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify(params)
    });

    request.done(function(res) {
      this.setState({
        snackbarMessage: ['Rated "', movie.title, '" with ', res.rating, ' stars!'].join('')
      });
      this.refs.ratedSnackbar.show();
      ackCallback(true)
    }.bind(this));

    request.fail(function(x) {
      console.log("rating failed");
      console.log(x);
      var e = x.responseJSON;
      this.setState({snackbarMessage: e.error + " (" + x.status + ")"});
      this.refs.ratedSnackbar.show();
      ackCallback(false);
    }.bind(this));
  },

  deleteRating: function(movie, ackCallback) {
    var request = $.ajax({
      method: 'DELETE',
      url: ['/movies/', movie.id, '/rating'].join(''),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8'
    });

    request.done(function() {
      this.setState({
        snackbarMessage: ['Deleted rating for movie "', movie.title].join('')
      });
      this.refs.ratedSnackbar.show();
      ackCallback(true)
    }.bind(this));

    request.fail(function(x) {
      console.log("rating failed");
      console.log(x);
      var e = x.responseJSON;
      this.setState({snackbarMessage: e.error + " (" + x.status + ")"});
      this.refs.ratedSnackbar.show();
      ackCallback(false);
    }.bind(this));
  },

  onSigninDone: function(e) {
    console.log('Done sign in');
    this.onAuthorized(e.token);
  },

  onSignupDone: function(e) {
    console.log('Done sign up');
    this.onAuthorized(e.session.token);
  },

  onAuthorized: function(token) {
    location.reload();
  },

  handleTabChange: function(value) {
    this.setState({
      tab: value
    })
  },

  render: function() {
    return (
      <div>

        <AuthDialog ref="authDialog"
                    onSigninDone={this.onSigninDone}
                    onSignupDone={this.onSignupDone}/>

        <Snackbar ref="ratedSnackbar"
                  message={this.state.snackbarMessage}
                  autoHideDuration={2000}
                  action="Ok"
                  style={{fontFamily: 'Roboto', fontSize: 'small'}}/>

        <div className="row end-xs">
          <div className="col-xs-3 start-xs" style={{width: "300px"}}>
            <UserAvatar user={this.state.user}
                        onClick={this.onUserAvatarClicked}/>
          </div>
        </div>

        <br/>
        <br/>

        <div className="row center-xs">
          <div className="col-md-11">
            <div className="start-xs">
              <Paper zDepth={2}>
                <Tabs valueLink={{value: this.state.tab, requestChange: this.handleTabChange}}>
                  <Tab style={MyTheme.tabStyle} label="Recommendations" value="recommendations"/>
                  <Tab style={MyTheme.tabStyle} label="Search" value="search"/>
                </Tabs>
              </Paper>
              {
                (this.state.tab == "recommendations")
                ? (
                  <div>
                    <Paper zDepth={0}>
                      <p style={{fontSize: "larger", color: MyTheme.palette.textColor}}>
                        Recommended for you
                      </p>
                      <RecommendationsTab user={this.state.user} onRated={this.onRated}/>
                      <br/>
                      <p style={{fontSize: "larger", color: MyTheme.palette.textColor}}>
                        Top rated
                      </p>
                      <PopularTab onRated={this.onRated}/>
                    </Paper>
                  </div>
                )
                : (
                  <div>
                    <SearchTab onReated={this.onRated}/>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </div>
    )
  }

});

ReactDOM.render(
  <Main />,
  document.getElementById('content')
);
