var React         = require('react'),
    Dialog        = require('material-ui/lib/dialog'),
    Tab           = require('material-ui/lib/tabs/tab'),
    Tabs          = require('material-ui/lib/tabs/tabs'),
    SignInForm    = require('./signin_form.jsx.js'),
    SignUpForm    = require('./signup_form.jsx.js'),
    Theme         = require('../theme-wrapper.jsx.js');

var AuthDialog = React.createClass({

  onSigninDone: function(e) {
    if (this.props && this.props.onSigninDone) {
      this.props.onSigninDone(e);
    }
    this.refs.dialog.dismiss();
  },

  onSignupDone: function(e) {
    if (this.props && this.props.onSignupDone) {
      this.props.onSignupDone(e);
    }
    this.refs.dialog.dismiss();
  },

  show: function() {
    this.refs.dialog.show();
  },

  render: function() {
    return (
      <Dialog ref="dialog" bodyStyle={{padding: "0px"}} >
        <div>
          <Tabs>
            <Tab style={Theme.tabStyle} label="Sign in">
              <SignInForm onSigninDone={this.onSigninDone} />
            </Tab>
            <Tab style={Theme.tabStyle} label="Sign up">
              <SignUpForm onSignupDone={this.onSignupDone} />
            </Tab>
          </Tabs>
        </div>
      </Dialog>
    )
  }

});

module.exports = AuthDialog;
