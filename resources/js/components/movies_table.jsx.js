var React = require('react'),
    IconButton = require('material-ui/lib/icon-button'),
    Table = require('material-ui/lib/table/table'),
    TableBody = require('material-ui/lib/table/table-body'),
    TableHeader = require('material-ui/lib/table/table-header'),
    TableHeaderColumn = require('material-ui/lib/table/table-header-column'),
    TableRow = require('material-ui/lib/table/table-row'),
    TableRowColumn = require('material-ui/lib/table/table-row-column'),
    LinkIcon = require('material-ui/lib/svg-icons/content/link'),
    MovieStars = require('./movie_stars.jsx.js');

var MoviesTable = React.createClass({

  onStarClicked: function(movieId) { return function(starIndex) {
    console.log(['movie ', movieId, ' rated with ', starIndex + 1, ' stars!'].join(''));
  }.bind(this)},

  render: function() {
    var movies = this.props.movies;
    return (
      <Table selectable={false}>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn tooltip='The Name'>Name</TableHeaderColumn>
            <TableHeaderColumn tooltip='Year' style={{textAlign: 'center'}}>Year</TableHeaderColumn>
            <TableHeaderColumn tooltip='Year' style={{textAlign: 'center'}}>My Rating</TableHeaderColumn>
            <TableHeaderColumn tooltip="IMDB" style={{textAlign: 'right'}}>IMDB</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
        {
          movies.map(function(movie) { return (
            <TableRow key={movie.id}>
              <TableRowColumn>{movie.title}</TableRowColumn>
              <TableRowColumn style={{textAlign: 'center'}}>{movie.year}</TableRowColumn>
              <TableRowColumn style={{textAlign: 'center'}}>
                <MovieStars numStars={0} onStarClicked={this.onStarClicked(movie.id)} />
              </TableRowColumn>
              <TableRowColumn style={{textAlign: 'right'}}>
                <IconButton href={movie.imdb}
                            linkButton={true}
                            primary={true}>
                  <LinkIcon/>
                </IconButton>
              </TableRowColumn>
            </TableRow>
          )}.bind(this))
        }
        </TableBody>
      </Table>
    )
  }

});

module.exports = MoviesTable;
