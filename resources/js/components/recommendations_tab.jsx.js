var React = require('react'),
    Snackbar = require('material-ui/lib/snackbar'),
    MovieCards = require('./movie_cards.jsx.js');

var RecommendationTab = React.createClass({

  handleUpdateClick: function() {
    this.setState({
      updateMessage: "Updating recommendations...",
      updatingNow: true
    });
    this.refs.updateSnackbar.show();

    var request = $.ajax({
      url: '/movies/recommended',
      method: 'PUT',
      dataType: 'json'
    });

    request.done(function (res) {
      this.setState({
        updateMessage: "Recommendations updated!",
        updatingNow: false
      });

      this.getRecommendations();
      this.refs.updateSnackbar.show();

      setTimeout(function () {
        this.refs.updateSnackbar.dismiss();
      }.bind(this), 2000);

    }.bind(this));

    request.fail(function (x) {
      this.setState({
        updateMessage: "Something went wrong...",
        updatingNow: false
      });

      this.refs.updateSnackbar.show();

      setTimeout(function () {
        this.refs.updateSnackbar.dismiss();
      }.bind(this), 2000);

    }.bind(this));
  },

  getRecommendations: function() {
    $.get('/movies/recommended', function(data) {
      var movies = data.map(function (d) {
        var movie = {
          rating: d.movie.rating,
          id: d.movie.id,
          title: d.movie.name,
          year: d.movie.year,
          imdb: "http://imdb.com"
        };

        if (d.movie.tmdbPosterPath) {
          movie.poster = "http://image.tmdb.org/t/p/w300" + d.movie.tmdbPosterPath.trim();
        } else {
          movie.poster = "/assets/images/poster_placeholder.png";
        }

        return movie

      }.bind(this));
      this.setState({movies: movies})
    }.bind(this))
  },

  componentDidMount: function() {
    this.getRecommendations()
  },

  getInitialState: function() {
    return {
      movies: [],
      updateMessage: "Recommendations are ready!",
      updatingNow: false
    };
  },

  render: function() {
    return (
      <div>

        <MovieCards movies={this.state.movies} onRated={this.props.onRated}/>

        <Snackbar ref="updateSnackbar"
                  message={this.state.updateMessage}
                  autoHideDuration={0}
                  action="Dismiss"
                  style={{fontFamily: 'Roboto', fontSize: 'small'}}/>

      </div>
    )
  }

});

module.exports = RecommendationTab;
