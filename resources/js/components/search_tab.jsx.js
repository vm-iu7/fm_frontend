var React = require('react'),
    Paper = require('material-ui/lib/paper'),
    TextField = require('material-ui/lib/text-field'),
    FlatButton = require('material-ui/lib/flat-button'),
    RaisedButton = require('material-ui/lib/raised-button'),
    MovieCards = require('./movie_cards.jsx.js');

var SearchTab = React.createClass({

  getInitialState: function() {
    return {
      movies: [],
      searchField: "",
      page: 0,
      moreResults: false
    };
  },

  handleSearchClick: function() {
    this.setState({
      movies: [],
      moreResults: false
    });
    this.makeSearchRequest(1);
    this.setState({
      page: 1
    })
  },

  handleMoreResultsClick: function(e) {
    this.makeSearchRequest(this.state.page + 1);
    this.setState({
      page: this.state.page + 1
    });
  },

  handleSearchFieldChange: function(e) {
    this.setState({searchField: e.target.value});
  },

  makeSearchRequest: function(page) {
    var searchTitle = this.refs.searchField.getValue();
    console.log("search for '" + searchTitle + "'");

    var request = $.ajax({
      method: 'GET',
      url: '/movies',
      data: {
        'title': searchTitle,
        'page': page,
        'page_len': 15
      }
    });

    request.done(function(movieData) {

      console.log(movieData);

      var newMovies = movieData.values.map(function (md) {
        var movie = {
          id: md.id,
          title: md.name,
          year: md.year,
          rating: md.rating,
          imdb: "http://imdb.com"
        };

        if (md.tmdbPosterPath) {
          movie.poster = "http://image.tmdb.org/t/p/w300" + md.tmdbPosterPath.trim();
        } else {
          movie.poster = "/assets/images/poster_placeholder.png";
        }

        return movie;
      });

      this.setState({
        movies: this.state.movies.concat(newMovies),
        moreResults: movieData.next ? true : false
      });

    }.bind(this));
  },

  render: function() {
    return (
      <t>
        <br/>
        <Paper zDepth={0}>
          <div className="row center-xs">
            <div className="col-xs-9 center-xs">
              <TextField hintText="Title.."
                         ref="searchField"
                         style={{width: "80%"}}>
              </TextField>
              <RaisedButton primary={true}
                            onClick={this.handleSearchClick}
                            label="search"
                            style={{marginLeft: "20px"}}>
              </RaisedButton>
            </div>
          </div>
        </Paper>
        <MovieCards movies={this.state.movies} onRated={this.props.onRated}/>
        <br/>
        <div className="row center-xs">
          <div className="col-xs-12">
            {
              this.state.moreResults
                ? <FlatButton primary={true}
                              label="show more results"
                              onClick={this.handleMoreResultsClick}/>
                : null
            }
          </div>
        </div>
      </t>
    )
  }
});

module.exports = SearchTab;
