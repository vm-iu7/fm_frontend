var React = require('react'),
    Avatar = require('material-ui/lib/avatar'),
    List = require('material-ui/lib/lists/list'),
    ListDivider = require('material-ui/lib/lists/list-divider'),
    ListItem = require('material-ui/lib/lists/list-item'),
    Paper = require('material-ui/lib/paper'),
    Colors = require('material-ui/lib/styles/colors'),
    Theme = require('../theme-wrapper.jsx.js');

var UserAvatar = React.createClass({

  render: function() {
    var user = this.props.user;
    var avatar = "?";
    if (user) {
      avatar = user.firstName[0];
    }
    else {
      user = {
        firstName: "Sign in",
        lastName: "In"
      };
    }

    console.log(this.props);

    return (
      //<List className="end-md box">
      //  <ListItem
      //    rightAvatar={
      //    }
      //    primaryText={user.firstName}
      //    onClick={this.props.onClick}/>
      //</List>
      <Paper className="end-md" zDepth={0}>
        <div style={{margin: "10px"}}>
          <a style={{marginRight: "25px", color: Colors.white}} onClick={this.props.onClick}>{user.firstName}</a>
          <span>
            <Avatar backgroundColor={Theme.palette.accent3Color}>{avatar}</Avatar>
          </span>
        </div>
      </Paper>
    )
  }

});

module.exports = UserAvatar;
