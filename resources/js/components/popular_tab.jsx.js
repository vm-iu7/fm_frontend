var React = require('react'),
    MovieCards = require('./movie_cards.jsx.js');

var PopularTab = React.createClass({

  componentDidMount: function() {
    $.get('/movies/top_rated?count=14', function(data) {
      var movies = data.map(function (d) {
        var movie =  {
          rating: d.movie.rating,
          id: d.movie.id,
          title: d.movie.name,
          year: d.movie.year,
          imdb: "http://imdb.com"
        };

        if (d.movie.tmdbPosterPath) {
          movie.poster = "http://image.tmdb.org/t/p/w300" + d.movie.tmdbPosterPath.trim();
          //movie.poster = d.movie.tmdbPosterPath.trim();
        } else {
          movie.poster = "/assets/images/poster_placeholder.png";
        }
        return movie;
      }.bind(this));
      this.setState({movies: movies})
    }.bind(this));
  },

  getInitialState: function() {
    return { movies: [] }
  },

  render: function() {
    return (
      <div>
        <MovieCards movies={this.state.movies} onRated={this.props.onRated}/>
      </div>
    )
  }

});

module.exports = PopularTab;
