var React = require('react'),
    Paper = require('material-ui/lib/paper'),
    RaisedButton = require('material-ui/lib/raised-button'),
    TextField = require('material-ui/lib/text-field');

var SigninForm = React.createClass({

  signInButtonClicked: function() {
    console.log('clicked signin button');

    var params = {
      email: this.refs.emailField.getValue(),
      password: this.refs.passwordField.getValue()
    };

    var request = $.ajax({
      method: 'POST',
      url: '/users/session',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify(params)
    });

    request.done(function (response) {
      console.log('sign up done');
      console.log(response);

      if (this.props.onSigninDone) {
        this.props.onSigninDone(response);
      }
    }.bind(this));
  },

  render: function() {
    return (
      <Paper zDepth={0}>

        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="emailField"
              style={{width: "90%"}}
              hintText="Your email"
              floatingLabelText="Email" />
          </div>
        </div>

        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="passwordField"
              style={{width: "90%"}}
              hintText="Your password"
              floatingLabelText="Password"
              type="password" />
          </div>
        </div>

        <br/>

        <div className="row">
          <div className="col-xs-11 end-xs">
            <RaisedButton label="Sign in"
                          primary={true}
                          onClick={this.signInButtonClicked} />
          </div>
        </div>

        <br/>
      </Paper>
    )
  }

});

module.exports = SigninForm;
