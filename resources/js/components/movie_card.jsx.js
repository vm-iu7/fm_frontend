var React = require('react'),
    ImageLoader = require('react-imageloader'),
    IconButton = require('material-ui/lib/icon-button'),
    LinkIcon = require('material-ui/lib/svg-icons/content/link'),
    Card = require('material-ui/lib/card/card'),
    CardText = require('material-ui/lib/card/card-text'),
    CardMedia = require('material-ui/lib/card/card-media'),
    MovieStars = require('./movie_stars.jsx.js');

var Colors = require('material-ui/lib/styles/colors');

var MovieCard = React.createClass({

  max_title_length: 25,
  posterPlaceholder: "/assets/images/poster_placeholder.png",

  initialRating: function() {
    var rating = 0;
    if (this.props.movie.rating) {
      rating = this.props.movie.rating;
    }
    return rating;
  },

  onRated: function(stars) {
    var ack = function(success) {
      if (!success) {
        console.log("movie card: rating request failed... rolling back");
        this.setState({ rating: this.initialRating() });
      } else {
        console.log("movie card: rating request succeed");
        console.log(this.state.rating);
        this.setState({ rating: stars });
      }
    }.bind(this);

    if (this.props.onRated) {
      this.setState({rating: stars});
      this.props.onRated(stars, this.props.movie, ack);
    }

    console.log("movie card: onRated: waiting for rating request");
    console.log(this.state.rating);
  },

  componentWillReceiveProps: function(nextProps) {
    var rating = 0;
    if (nextProps.movie.rating) {
      rating = nextProps.movie.rating;
    }
    this.setState({
      poster: nextProps.movie.poster,
      rating: rating
    });
  },

  getInitialState: function() {
    return {
      rating: this.initialRating(),
      poster: this.props.movie.poster
    }
  },

  onImageLoadFailed: function() {
    console.warn("failed to load poster");
    this.setState({
      poster: this.posterPlaceholder
    })
  },

  imagePreloader: function() {
    return (
      <img src="/assets/images/poster_placeholder.png" width="250"/>
    )
  },

  render: function() {
    var movie = this.props.movie;
    var titleTrunc = this.props.movie.title;
    if (titleTrunc.length > this.max_title_length) {
      titleTrunc = titleTrunc.slice(0, this.max_title_length) + "..";
    }

    var numStars = this.state.rating;

    return (
      <Card style={{width: "250px", margin: "5px"}} className="start-xs">

        <CardMedia
          tooltip="Sort"
          overlay={
            <CardText>
              <p style={{fontWeight: "bold"}} tooltip={movie.title}>{titleTrunc}</p>
              <p style={{color: Colors.grey500}}>{movie.year}</p>
              <div className="row">
                <div className="col-xs-9 start-xs">
                  <MovieStars numStars={numStars} onStarClicked={this.onRated}/>
                </div>
                <div className="col-xs-3 end-xs">
                  <IconButton href={movie.imdb}
                              linkButton={true}
                              primary={true}
                              tooltip="Link to IMDB">
                    <LinkIcon color={Colors.grey100}/>
                  </IconButton>
                </div>
              </div>
            </CardText>
          }>

          <ImageLoader
            src={this.state.poster}
            imgProps={{width: "250"}}
            preloader={this.imagePreloader}
          >
            <img src="/assets/images/poster_placeholder.png" width="250"/>
          </ImageLoader>
        </CardMedia>

      </Card>
    )
  }

});

module.exports = MovieCard;
