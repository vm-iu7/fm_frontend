var React = require('react'),
    TransitionGroup = require('react-addons-css-transition-group'),
    MovieCard = require('./movie_card.jsx.js');

var MovieCards = React.createClass({

  render: function() {
    var movies = this.props.movies;
    return (
        <TransitionGroup className="row center-xs" transitionName="fade"
                         transitionAppearTimeout={500} transitionLeaveTimeout={500}>
        {
          movies.map(function(m) {
            return (
              <div className="col" key={m.id}>
                <MovieCard movie={m} onRated={this.props.onRated}/>
              </div>
            );
          }.bind(this))
        }
        </TransitionGroup>
    )
  }

});

module.exports = MovieCards;
