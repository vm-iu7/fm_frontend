var React = require('react'),
    Paper = require('material-ui/lib/paper'),
    RaisedButton = require('material-ui/lib/raised-button'),
    TextField = require('material-ui/lib/text-field');

var SignUpForm = React.createClass({

  signUpButtonClicked: function() {
    console.log('clicked sign up button');
    var params = {
      email: this.refs.emailField.getValue(),
      firstName: this.refs.firstNameField.getValue(),
      lastName: this.refs.lastNameField.getValue(),
      password: this.refs.passwordField.getValue()
    };

    console.log(params);
    console.log(JSON.stringify(params));

    var request = $.ajax({
      method: 'POST',
      url: '/users',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      data: JSON.stringify(params)
    });

    request.done(function (response) {
      console.log('sign up done');
      console.log(response);
      console.log(this.props.onSignupDone);
      if (this.props && this.props.onSignupDone) {
        this.props.onSignupDone(response);
      }
    }.bind(this));
  },

  render: function() {
    return (
      <Paper zDepth={0}>
        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="emailField"
              style={{width: "90%"}}
              hintText="Your email"
              floatingLabelText="Email" />
          </div>
        </div>

        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="firstNameField"
              style={{width: "90%"}}
              hintText="Your first name"
              floatingLabelText="First name" />
          </div>
        </div>

        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="lastNameField"
              style={{width: "90%"}}
              hintText="Your last name"
              floatingLabelText="Last name" />
          </div>
        </div>

        <div className="row center-xs">
          <div className="col-xs-11">
            <TextField
              ref="passwordField"
              style={{width: "90%"}}
              hintText="Your password"
              floatingLabelText="Password"
              type="password" />
          </div>
        </div>

        <br/>

        <div className="row">
          <div className="col-xs-11 end-xs">
            <RaisedButton label="Sign up"
                          primary={true}
                          onClick={this.signUpButtonClicked} />
          </div>
        </div>

        <br/>
      </Paper>
    )
  }

});

module.exports = SignUpForm;
