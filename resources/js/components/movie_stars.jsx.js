var React = require('react'),
    Colors = require('material-ui/lib/styles/colors'),
    StarIcon = require('material-ui/lib/svg-icons/toggle/star');

var MovieStars = React.createClass({

  getInitialState: function() {
    return { stars: this.props.numStars };
  },

  onMouseEnterStar: function(starIndex) { return function(e) {
    // console.log(['mouse entered star #', starIndex].join(''));

    if ((starIndex + 1) == this.state.stars) {
      this.setState({ stars: 0 })
    } else {
      this.setState({ stars: starIndex + 1 });
    }

  }.bind(this)},

  onMouseLeaveStar: function(starIndex) { return function(e) {
    // console.log(['mouse left star #', starIndex].join(''));
    this.setState({ stars: this.props.numStars });
  }.bind(this)},

  onCLickedStar: function(starIndex) { return function(e) {
    // console.log(['star #', starIndex, 'clicked'].join(''));
    this.props.onStarClicked(this.state.stars);
  }.bind(this)},

  componentWillReceiveProps: function(nexProps) {
    this.setState({stars: nexProps.numStars})
  },

  render: function() {
    var numStars = this.state.stars;
    // console.log('rendering stars with numStars=', numStars)
    return (
      <t>
      {
        Array.apply(null, {length: 5}).map(Number.call, Number).map(function (i) {
          if (i < numStars) {
            return (
              <a href="#!"
                 onMouseEnter={this.onMouseEnterStar(i)}
                 onMouseLeave={this.onMouseLeaveStar(i)}
                 key={i}>
                <StarIcon color={Colors.grey100}
                          onClick={this.onCLickedStar(i)}/>
              </a>
            )
          }
          else {
            return (
              <a href="#!"
                 onMouseEnter={this.onMouseEnterStar(i)}
                 onMouseLeave={this.onMouseLeaveStar(i)}
                 key={i}>
                <StarIcon color={Colors.grey700}
                          onClick={this.onCLickedStar(i)}/>
              </a>
            )
          }
        }.bind(this))
      }
      </t>
    )
  }

});

module.exports = MovieStars;
