var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    react = require('react');

var path = {
    js: {
        src: 'resources/js/',
        dest: 'public/javascripts'
    },
    css: {
        src: 'resources/css/'
    },
    html: {
        src: 'app/views/html/'
    }
};

gulp.task('browserify', function() {
    gulp.src(path.js.src + 'main.jsx.js')
        .pipe(browserify({ transform: ['reactify'] }))
        .on('error', function(error) {
          console.log(error.message);
          this.emit('end');
        })
        .pipe(gulp.dest(path.js.dest));
});

gulp.task('watch', function() {
    gulp.watch(path.js.src + '**/*.js', [ 'browserify' ]);
});

gulp.task('build', ['browserify']);

gulp.task('default', ['browserify', 'watch']);
